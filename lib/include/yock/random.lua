-- Copyright 2023 The Yock Authors. All rights reserved.
-- Use of this source code is governed by a MIT-style
-- license that can be found in the LICENSE file.

---@meta _

---@class random
random = {}

---@param n number
---@return string
function random.str(n) end

---@return number
function random.port() end
